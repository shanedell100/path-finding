# import packages
import argparse


# Node class
class Node:
    def __init__(self, character=" ", x=-1, y=-1, parent_x=0, parent_y=0):
        self.character = character
        self.x = x
        self.y = y
        self.f = 0
        self.g = 0
        self.h = 0
        self.parent_x = parent_x
        self.parent_y = parent_y


# Barber class
class Barbers:
    def __init__(self, time_step=0, x=0, y=0):
        self.time_step = time_step
        self.x = x
        self.y = y


# Global variables for different parts of the map
mapX, mapY, start_x1, start_y1, goal_x, goal_y = 0, 0, 0, 0, 0, 0
new_start_x, new_start_y = 0, 0
goal_reached = False
wall_number = 0
barb_number = 0
wallX = [None] * 300
wallY = [None] * 300
barber_array = [Barbers() for i in range(300)]
neighbor_open_index, neighbor_closed_index = 0, 0
time_step = 0
astar_success = False


# Function to create and fill a map
def create_and_fill_map(start_x, start_y, goal_x, goal_y):
    # Import global variables
    global mapX, mapY, start_x1, start_y1, new_start_x, new_start_y
    global wall_number, barb_number, wallX, wallY, barber_array
    global neighbor_open_index, neighbor_closed_index
    global time_step, astar_success, goal_reached

    # Create map and convert certain variables into ints
    temp_map = [[Node() for i in range(0, int(mapY))]
                for j in range(0, int(mapX))]
    start_x = int(start_x)
    start_y = int(start_y)
    goal_x = int(goal_x)
    goal_y = int(goal_y)

    # Create start
    temp_map[start_x][start_y].character = "S"
    temp_map[start_x][start_y].x = start_x
    temp_map[start_x][start_y].y = start_y

    # Create end
    temp_map[goal_x][goal_y].character = "G"
    temp_map[goal_x][goal_y].x = goal_x
    temp_map[goal_x][goal_y].y = goal_y

    # For statement to put every wall onto the map
    for i in range(0, wall_number):
        x, y = 0, 0
        x = int(wallX[i])
        y = int(wallY[i])

        temp_map[x][y].character = "W"
        temp_map[x][y].x = x
        temp_map[x][y].y = y

    # For statement to put every barber onto the map
    for i in range(0, barb_number):
        x, y
        x = int(barber_array[i].x)
        y = int(barber_array[i].y)

        if (temp_map[x][y].character == "W" or temp_map[x][y].character == "B"
                or temp_map[x][y].character == "G"
                or temp_map[x][y].character == "S"
                or temp_map[x][y].character == "*"):
            continue

        temp_map[x][y].character = "B"
        temp_map[x][y].x = x
        temp_map[x][y].y = y

    return temp_map  # return map


# Function to print and then clear the map
def print_and_clear(map, start_x, start_y):
    # Import global variable
    global goal_x, goal_y, time_step, mapX, mapY, barber_array

    # Print the map
    for i in range(0, int(mapX)):
        for j in range(0, int(mapY)):
            if j != 7:
                print(map[i][j].character, end=" ")
            elif j == 7 and i == 0:
                print("{0} \t \t Time Step: {1}".format(map[i][j].character, time_step))
            else:
                print(map[i][j].character)

    time_step += 1  # increment time step

    # Clear the map
    for i in range(0, int(mapY)):
        for i in range(0, int(mapX)):
            map[i][j].character = ' '


# Fucntion to find the open neighbor index
def neighor_in_open_list(open_list, size, x, y):
    global neighbor_open_index

    for i in range(0, size):
        if open_list[i].x == x and open_list[i].y == y:
            neighbor_open_index = i
            return True
    return False


# Function to find closed neighbor index
def neighbor_in_closed_list(closed_list, size, x, y):
    global neighbor_closed_index

    for i in range(0, size):
        if closed_list[i].x == x and closed_list[i].y == y:
            neighbor_closed_index = i
            return True
    return False


# Check to see if the open list is empty
def check_open_list(open_list):
    for i in range(0, 75):
        if open_list[i].x != -1:
            return True
    return False


# Function to do a star algorith
def astar_search(start_x, start_y, goal_x, goal_y):
    # Import global variables
    global mapX, mapY, start_x1, start_y1, new_start_x, new_start_y
    global wall_number, barb_number, wallX, wallY, barber_array
    global neighbor_open_index, neighbor_closed_index
    global time_step, astar_success, goal_reached

    # Create map
    map = create_and_fill_map(start_x, start_y, goal_x, goal_y)
    start_x = int(start_x)
    start_y = int(start_y)
    goal_x = int(goal_x)
    goal_y = int(goal_y)

    # Create open list
    open_list = [Node() for i in range(0, 75)]
    curr_open_size = 0

    # Create closed list
    closed_list = [Node() for i in range(0, 75)]
    curr_closed_size = 0

    # curr node
    curr = Node()

    # Total moves
    num_of_moves = 0

    # G and h values
    g, h = 0, 0

    open_list[curr_open_size] = map[start_x][start_y]

    # Calculate h
    h = (abs(goal_x - open_list[curr_open_size].x) + abs(goal_y - open_list[curr_open_size].y))

    # Store h, g, f
    g = 0
    open_list[curr_open_size].g = g
    open_list[curr_open_size].h = h
    open_list[curr_open_size].f = h + g

    curr_open_size += 1

    # Loop till open list is empty
    while check_open_list(open_list):
        best_f = 100
        best_index = -1

        # Find best f value
        for i in range(0, curr_open_size):
            if open_list[i].x != -1:
                curr_f = open_list[i].f

                if curr_f < best_f:
                    best_f = curr_f
                    best_index = i

        curr = open_list[best_index]

        num_of_moves += 1

        # See if curr node is the goal
        goal_pos1 = curr.x
        goal_pos2 = curr.y
        if goal_x == goal_pos1 and goal_pos2 == goal_y:
            match = 0
            while (curr.x != start_x) or (curr.y != start_y):
                for i in range(0, curr_closed_size):
                    if closed_list[i].x == curr.parent_x and closed_list[i].y == curr.parent_y:
                        match = i

                new_start_x = curr.x
                new_start_y = curr.y

                curr = closed_list[match]

                # Check to see where the path needs to go
                if map[curr.x][curr.y].x == map[start_x + 1][start_y].x and map[curr.x][curr.y].y == map[start_x + 1][start_y].y:
                    map[curr.x][curr.y].character = "*"
                    new_start_x = curr.x
                    new_start_y = curr.y
                elif map[curr.x][curr.y].x == map[start_x - 1][start_y].x and map[curr.x][curr.y].y == map[start_x - 1][start_y].y:
                    map[curr.x][curr.y].character = "*"
                    new_start_x = curr.x
                    new_start_y = curr.y
                elif map[curr.x][curr.y].x == map[start_x][start_y + 1].x and map[curr.x][curr.y].y == map[start_x + 1][start_y + 1].y:
                    map[curr.x][curr.y].character = "*"
                    new_start_x = curr.x
                    new_start_y = curr.y
                elif map[curr.x][curr.y].x == map[start_x][start_y - 1].x and map[curr.x][curr.y].y == map[start_x][start_y - 1].y:
                    map[curr.x][curr.y].character = "*"
                    new_start_x = curr.x
                    new_start_y = curr.y

                start_x1 = int(start_x1)
                start_y1 = int(start_y1)

                # Make sure start node doesn't get overriden
                if map[curr.x][curr.y].x == map[start_x1][start_y1].x and map[curr.x][curr.y].y == map[start_x1][start_y1].y:
                    map[curr.x][curr.y].character = "S"
                astar_success = True

        # Chane goal back to what it was if over riden
        if map[curr.x][curr.y].x == map[goal_x][goal_y].x and map[curr.x][curr.y].y == map[goal_x][goal_y].y:
            map[curr.x][curr.y].character = "G"
            goal_reached = True

        # If a star works break out of loop
        if astar_success:
            break

        # For loop to take curr node from the open list to the closed list
        for i in range(0, curr_open_size):
            if open_list[i].x == -1:
                continue

            if open_list[i].x == curr.x and open_list[i].y == curr.y:
                closed_list[curr_closed_size] = curr
                curr_closed_size += 1

                open_list.pop(i)
                replacement = Node()
                open_list.append(replacement)

        # Create array to hold neighbors
        neighbors = [Node() for i in range(0, 4)]
        neighbors_number = 0

        # Variables to hold the curr position values
        curr_x = curr.x
        curr_y = curr.y

        # Check Moves
        # Move Up
        if (curr_y - 1) >= 0:
            if map[curr_x][curr_y - 1].character != "W" and map[curr_x][curr_y - 1].character != "B":
                neighbors[neighbors_number] = map[curr_x][curr_y - 1]
                neighbors[neighbors_number].x = curr_x
                neighbors[neighbors_number].y = curr_y - 1
                neighbors_number += 1

        # Move down
        if (curr_y + 1) < int(mapY):
            if map[curr_x][curr_y + 1].character != "W" and map[curr_x][curr_y + 1].character != "B":
                neighbors[neighbors_number] = map[curr_x][curr_y + 1]
                neighbors[neighbors_number].x = curr_x
                neighbors[neighbors_number].y = curr_y + 1
                neighbors_number += 1

        # Move Left
        if (curr_x - 1) >= 0:
            if map[curr_x - 1][curr_y].character != "W" and map[curr_x - 1][curr_y].character != "B":
                neighbors[neighbors_number] = map[curr_x - 1][curr_y]
                neighbors[neighbors_number].x = curr_x - 1
                neighbors[neighbors_number].y = curr_y
                neighbors_number += 1

        # Move Right
        if (curr_x + 1) < int(mapX):
            if map[curr_x + 1][curr_y].character != "W" and map[curr_x + 1][curr_y].character != "B":
                neighbors[neighbors_number] = map[curr_x + 1][curr_y]
                neighbors[neighbors_number].x = curr_x + 1
                neighbors[neighbors_number].y = curr_y
                neighbors_number += 1

        # For loop to check all neighbor nodes
        for i in range(0, neighbors_number):
            curr_move = curr.g

            x = neighbors[i].x
            y = neighbors[i].y

            neighbor_check_closed = neighbor_in_closed_list(closed_list, curr_closed_size, x, y)
            neighbor_ck_open = neighor_in_open_list(open_list, curr_open_size, x, y)

            if neighbor_check_closed:
                continue
            elif neighbor_ck_open:
                neighbors[i].g = num_of_moves

                if neighbors[i].g < open_list[neighbor_open_index].g:
                    index = 0

                    for j in range(0, curr_open_size):
                        if open_list[j] == curr:
                            index = j

                    open_list.pop(index)

                    h = abs(goal_x - neighbors[i].x) + abs(goal_y - neighbors[i].y)

                    neighbors[i].h = h
                    neighbors[i].g = num_of_moves
                    neighbors[i].f = curr_move + h

                    neighbors[i].parent_x = curr.x
                    neighbors[i].parent_y = curr.y

                    index2 = 0

                    for j in range(0, curr_open_size):
                        if open_list[j] == neighbors[i]:
                            index2 = j

                    open_list[index2] = neighbors[i]
            else:
                neighbors[i].parent_x = curr.x
                neighbors[i].parent_y = curr.y

                h = abs(goal_x - neighbors[i].x) + abs(goal_y - neighbors[i].y)

                neighbors[i].g = num_of_moves
                neighbors[i].h = h
                neighbors[i].f = curr_move + 1 + neighbors[i].h

                open_list[curr_open_size] = neighbors[i]
                curr_open_size += 1

    # If a star worked print the map of print no path found
    if astar_success:
        print_and_clear(map, start_x, start_y)
    else:
        if goal_reached:
            return
        print("No path found!")
        astar_success = False


# Function to get all necessary data
def get_data(map_file=None, barb_file=None):
    # Import global variables
    global mapX, mapY, start_x1, start_y1, goal_x, goal_y
    global wall_number, barb_number, time_step, new_start_x, new_start_y
    global astar_success, goal_x, goal_y, start_x1, start_y1, goal_reached

    # Prompt user for the name of the map and barb files
    if map_file is None and barb_file is None:
        map_file = input("Enter in map file: ")
        barb_file = input("Enter in barb file: ")

    # Open map and barb files
    open_map = open(map_file, 'r')
    open_barb = open(barb_file, 'r')

    index = 0  # variable to keep track of what letter we are on

    # For each line in map find the character and positions necessary
    for line in open_map:
        entry = line.split(" ")
        character = entry[0].upper()

        if index == 0:
            mapX = entry[1]
            mapY = entry[2]
            index = index + 1

        # If character if e closed the map and break out loop
        if character == "E":
            open_map.close()
            break

        if character == "S":
            start_x1 = entry[1]
            start_y1 = entry[2]

        if character == "G":
            goal_x = entry[1]
            goal_y = entry[2]

        if character == "W":
            wallX[wall_number] = entry[1]
            wallY[wall_number] = entry[2]
            wall_number = wall_number + 1

    calls = 0  # variable for the number of calls made to the search
    barb_entries = open_barb.readlines()  # array of the lines in the barb file

    count = 0  # Variable for the number of times looped through

    # For each line in the barber file find the time step and
    # positions and do necessary procedures
    for line in barb_entries:
        entry = line.split()
        barber_array[barb_number].time_step = entry[0]

        if barber_array[barb_number].time_step != str(time_step):
            if barber_array[barb_number].time_step != "-1" and count < 1:
                barber_array[barb_number].x = entry[1]
                barber_array[barb_number].y = entry[2]
                barb_number = barb_number + 1
                count += 1
                continue
            if calls == 0:
                astar_search(start_x1, start_y1, goal_x, goal_y)
                calls += 1
                count = 0
            else:
                astar_success = False
                astar_search(new_start_x, new_start_y, goal_x, goal_y)
                count = 0

        if len(entry) == 3 and barber_array[barb_number] != -1:
            barber_array[barb_number].x = entry[1]
            barber_array[barb_number].y = entry[2]
            barb_number = barb_number + 1


# Main Function
def main(map_file=None, barb_file=None):
    # Import global variables
    global astar_success, goal_x, goal_y, start_x1, start_y1
    global new_start_x, new_start_y, goal_reached, time_step

    # Call get data
    get_data(map_file, barb_file)

    # Loop till full is found
    while True:
        astar_success = False
        astar_search(new_start_x, new_start_y, goal_x, goal_y)
        if goal_reached:
            print("Path found in {0} time steps".format(time_step))
            return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--map_file", default=None)
    parser.add_argument("-b", "--barb_file", default=None)
    args = parser.parse_args()
    main(map_file=args.map_file, barb_file=args.barb_file)
